import 'dart:io';

class Tool {
  String id, name, description, image, status;
  int amount;

  Tool(this.id,
      this.name,
      this.description,
      this.image,
      this.status,
      this.amount);

  factory Tool.fromJson(Map<String, dynamic> json) {
    return Tool(
        json["id"],
        json["name"],
        json["description"],
        json["image"],
        json["status"],
        json["amount"]);
  }
}

class ToolCreateModel {
  String name, description;
  int amount;
  File image;

  ToolCreateModel(this.name, this.description, this.image, this.amount);
}
class ToolUpdateModel {
  String name, description;
  int amount;
  File image;

  ToolUpdateModel(this.name, this.description, this.image, this.amount);
}
