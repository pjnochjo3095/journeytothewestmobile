import 'dart:io';

class Actor{
  String id,name,decs,image,phone,email;
  bool status;
  Actor({
    this.id,
    this.name,
    this.decs,
    this.image,
    this.phone,
    this.email,
    this.status,
  });

  factory Actor.fromJson(Map<String, dynamic> json){
    return Actor(
        id: json["id"],
        name: json["name"],
        decs: json["description"],
        image: json["image"],
        phone: json["phone"],
        email: json["email"],
        status: json["status"],
    );
  }
}
class ActorUpdate{
  String id,desc,phone,email,name;
  File avatar;

  ActorUpdate(this.id, this.desc, this.phone, this.email, this.avatar,this.name);
}

