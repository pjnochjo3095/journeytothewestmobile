class Scene {
  String id, name, desc, location, status;
  String begin, end;
  int snapshot;

  Scene(
      {this.id,
      this.name,
      this.desc,
      this.location,
      this.begin,
      this.end,
      this.status,
      this.snapshot});

  factory Scene.fromJson(Map<String, dynamic> json) {
    return Scene(
        id: json["id"],
        name: json["name"],
        begin: json["time_start"].toString().split(" ")[0],
        end: json["time_end"].toString().split(" ")[0],
        desc: json["description"],
        status: json["status"],
        location: json["location"],
        snapshot: json["snapshot"]);
  }
}

class SceneCreateModel {
  String name, desc, location;
  DateTime begin, end;
  int snapshot;

  SceneCreateModel(
      this.name, this.desc, this.location, this.begin, this.end, this.snapshot);
}

class SceneUpdateModel {
  String id, name, desc, location;
  DateTime begin, end;
  int snapshot;

  SceneUpdateModel(
      {this.id,
      this.name,
      this.desc,
      this.location,
      this.begin,
      this.end,
      this.snapshot});

  factory SceneUpdateModel.fromJson(Map<String, dynamic> json) {
    return SceneUpdateModel(
        id: json["id"],
        name: json["name"],
        begin: json["time_start"],
        end: json["time_end"],
        desc: json["description"],
        location: json["location"],
        snapshot: json["snapshot"]);
  }
}
