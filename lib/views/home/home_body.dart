import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:journeytothewest/helpers/Constant.dart';
import 'package:journeytothewest/view_models/ActorViewModel.dart';
import 'package:journeytothewest/view_models/HomeViewModel.dart';
import 'package:journeytothewest/views/Component/category_card.dart';
import 'package:journeytothewest/views/actor/detail/actor_detail_screen.dart';
import 'package:journeytothewest/views/component/search_box.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../bottom_nav_bar.dart';

class HomeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return ScopedModelDescendant<HomeViewModel>(
      builder: (context, child, model) {
        if (model.isCheck) {
          return Stack(
            children: <Widget>[
              SafeArea(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: kDefaultPadding),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Container(
                            alignment: Alignment.center,
                            height: 52,
                            width: 52,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: kBackgroundColor),
                            child: model.currentUser.role.contains("User")
                                ? FlatButton(
                                    child: SvgPicture.asset(
                                      "assets/icons/user-icon.svg",
                                      color: Colors.black,
                                    ),
                                    onPressed: () {
                                      Navigator.of(context)
                                          .push(MaterialPageRoute(
                                        builder: (context) => ActorDetailScreen(
                                          model: ActorViewModel(),
                                          id: model.currentUser.id,
                                        ),
                                      ));
                                    },
                                    shape: RoundedRectangleBorder(
                                        side: BorderSide(
                                            width: 1, style: BorderStyle.solid),
                                        borderRadius:
                                            BorderRadius.circular(50)),
                                  )
                                : Container(),
                          ),
                        ),
                      ),
                      Text(
                        model.currentUser.isActor
                            ? "Hi " + model.currentUser.name
                            : "Hi " + model.currentUser.username,
                        style: TextStyle(
                          color: kTextColor,
                          fontSize: 30,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SearchBox(),
                      Flexible(
                        child: GridView.count(
                          crossAxisCount: 2,
                          childAspectRatio: 0.95,
                          crossAxisSpacing: 15,
                          mainAxisSpacing: 15,
                          children: model.myList.map((data) {
                            return CategoryCard(
                              title: data.title,
                              svgSrc: data.img,
                              press: () {
                                model.changeScreen(data.screen, context);
                              },
                              color1: data.color1,
                              color2: data.color2,
                            );
                          }).toList(),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }
}
