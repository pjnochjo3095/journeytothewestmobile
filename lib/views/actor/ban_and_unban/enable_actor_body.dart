import 'package:flutter/material.dart';
import 'package:journeytothewest/helpers/Constant.dart';
import 'package:journeytothewest/models/Actor.dart';
import 'package:journeytothewest/view_models/ActorViewModel.dart';
import 'package:journeytothewest/views/Component/search_box.dart';
import 'package:journeytothewest/views/Component/wukhong_background.dart';
import 'package:journeytothewest/views/component/round_btn.dart';
import 'package:scoped_model/scoped_model.dart';

class EnableActorBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return ScopedModelDescendant<ActorViewModel>(
        builder: (context, child, model) {
          if(!model.statusInit){
            model.fetchActors();
          }
          return WukongBackground(
        child: SafeArea(
          bottom: false,
          child: Column(
            children: <Widget>[
              SearchBox(),
              SizedBox(
                height: kDefaultPadding / 2,
              ),
              Expanded(
                child: Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 50),
                      decoration: BoxDecoration(
                        color: kBackgroundColor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(50),
                            topRight: Radius.circular(40)),
                      ),
                    ),
                    ListView.builder(
                      itemCount: model.currentListActors.length,
                      itemBuilder: (context, index) => ActorCard(itemIndex: index, actor: model.currentListActors[index], model: model,),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}

class ActorCard extends StatelessWidget {
  const ActorCard({
    Key key, this.itemIndex, this.actor, this.model
  }) : super(key: key);
  final int itemIndex;
  final Actor actor;
  final ActorViewModel model;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ScopedModelDescendant<ActorViewModel>(
      builder: (context, child, model) {
        return Container(
          margin: EdgeInsets.symmetric(
              horizontal: kDefaultPadding,
              vertical: kDefaultPadding / 2),
          height: 160,
          child: Stack(
            children: <Widget>[
              Container(
                height: 120,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(22),
                    color: itemIndex % 2 == 0
                        ? kCardSceneColor
                        : kCardSceneColor2,
                    boxShadow: [kDefaultShadow]),
                child: Container(
                  margin: EdgeInsets.only(right: 15),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(22)),
                ),
              ),
              Positioned(
                left: 10,
                top: 10,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: size.width * 0.45,
                      child: Text(
                        "Actor",
                        style: TextStyle(
                            color: kTextColor, fontSize: 16),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      width: size.width * 0.45,
                      child: Text(
                        actor.name,
                        style: TextStyle(
                            color: kTextColor, fontSize: 20),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      width: size.width * 0.4,
                      child: RoundBtn(
                        title: actor.status? "Enable":"Disable",
                        press: (){
                          model.EnableActor(actor.id, context);
                        },
                        color: actor.status ? Colors.green : Colors.red,
                        textColor: kTextColor,
                        font: 10,
                      ),
                    )
                  ],
                ),
              ),
              Positioned(
                right: 20,
                top: 10,
                child: Container(
                    width: 80,
                    height: 80,
                    alignment: Alignment.center,
                    child: FractionalTranslation(
                      translation: Offset(0 , 0.1),
                      child: Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                                    "assets/images/avatar/1.jpg"),
                                fit: BoxFit.cover),
                            borderRadius:
                            BorderRadius.circular(35),
                            boxShadow: [kDefaultShadow]),
                      ),
                    )),
              ),

            ],
          ),
        );
      },
    );
  }
}
