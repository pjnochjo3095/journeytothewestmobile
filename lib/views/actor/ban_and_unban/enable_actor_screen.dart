import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:journeytothewest/view_models/ActorViewModel.dart';
import 'package:journeytothewest/views/actor/ban_and_unban/enable_actor_body.dart';
import 'package:scoped_model/scoped_model.dart';

class EnableActorScreen extends StatelessWidget {
  ActorViewModel model;

  EnableActorScreen({this.model});

  @override
  Widget build(BuildContext context) {
    return ScopedModel<ActorViewModel>(
      model: model,
      child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            title: Text("Enable Actor", style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white,
                fontSize: 32),),
            actions: <Widget>[
              IconButton(
                icon: SvgPicture.asset("assets/icons/user-icon.svg"),
                onPressed: () {},
              )
            ],
          ),
          body: EnableActorBody(),
    ),);
  }
}
