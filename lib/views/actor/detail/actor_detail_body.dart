import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:journeytothewest/view_models/ActorViewModel.dart';
import 'package:scoped_model/scoped_model.dart';

class ActorDetailBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ScopedModelDescendant<ActorViewModel>(
      builder: (context, child, model) {
        if(model.currentActor != null){
          return Scaffold(
            body: Column(
              children: <Widget>[
                Container(
                  width: size.width,
                  height: size.height * 0.4,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/Piggy_Pink.jpg"),
                          fit: BoxFit.cover)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          width: 150,
                          height: 150,
                          decoration: BoxDecoration(shape: BoxShape.circle,
                          image: DecorationImage(image: NetworkImage(model.currentActor.image))),
                      ),
                      SizedBox(height:size.height * 0.03),
                      Container(
                        child: Text(model.currentActor.name,style: TextStyle(fontSize: 24,fontWeight: FontWeight.w700),),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        }
        return Container();

      },
    );
  }
}
