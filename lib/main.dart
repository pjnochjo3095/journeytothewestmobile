
import 'package:flutter/material.dart';
import 'package:journeytothewest/helpers/Constant.dart';
import 'package:journeytothewest/view_models/LoginViewModel.dart';
import 'package:journeytothewest/view_models/SignUpViewModel.dart';
import 'package:journeytothewest/views/login/welcome_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Journey To The West',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        textTheme: Theme.of(context).textTheme.apply(displayColor: kTextColor),
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primaryColor: kPrimaryColor
      ),
      home: WelcomeScreen(LoginViewModel(), SignUpViewModel()),
//        home:SceneScreen(SceneViewModel())
    );
  }
}









