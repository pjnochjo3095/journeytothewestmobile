



import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:journeytothewest/models/Actor.dart';
import 'package:journeytothewest/repos/ActorRepository.dart';
import 'package:scoped_model/scoped_model.dart';

class ActorViewModel extends Model {
  List<Actor> currentListActors = new List<Actor>();
  Actor currentActor;
  String id;

  bool statusInit = false;
  void fetchActors() async {
    ActorRepository actorRepository = new ActorRepositoryImp();
    actorRepository.fetchActor().then((value) {
      value.forEach((element) {
        currentListActors.add(element);
        notifyListeners();
      });
    });
    statusInit = true;
  }
  void EnableActor(String id, BuildContext context) async{
    ActorRepository actorRepository = new ActorRepositoryImp();
    actorRepository.EnableActor(id).then((value){
      if(value){
        Fluttertoast.showToast(
            msg: "Change Success",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            fontSize: 16.0);
        currentListActors.clear();
        statusInit = false;
        notifyListeners();
//        Navigator.of(context).pop();
//        Navigator.push(context, MaterialPageRoute(builder: (context) {
//          return EnableActorScreen(model: ActorViewModel(),);
//        }));
      }
    });
  }
  void getActorById(String id){
    ActorRepository actorRepository = ActorRepositoryImp();
    actorRepository.GetActorById(id).then((value){
      if(value != null){
        currentActor = value;
        notifyListeners();
      }
    });
  }
}


