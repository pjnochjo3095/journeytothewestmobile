import 'package:flutter/material.dart';
import 'package:journeytothewest/models/User.dart';
import 'package:journeytothewest/repos/UserRepository.dart';
import 'package:journeytothewest/view_models/HomeViewModel.dart';
import 'package:journeytothewest/views/home/home_screen.dart';
import 'package:scoped_model/scoped_model.dart';

class LoginViewModel extends Model {
  String username, password;

  void onChange(String tagret, String value) {
    switch (tagret) {
      case "username":
        this.username = value;
        break;
      case "password":
        this.password = value;
        break;
    }
    notifyListeners();
  }

  void clickToLogin(BuildContext context) async {
    UserLoginModel model = UserLoginModel(this.username, this.password);
    UserRepository userRepository = UserRepositoryImp();
    if (await userRepository.login(model) != null) {
      Navigator.of(context).pop();
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => HomeScreen(model:HomeViewModel(),)));
    }
  }
}
