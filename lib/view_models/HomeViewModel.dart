import 'package:flutter/material.dart';
import 'package:journeytothewest/helpers/Constant.dart';
import 'package:journeytothewest/models/User.dart';
import 'package:journeytothewest/repos/UserRepository.dart';
import 'package:journeytothewest/view_models/ActorViewModel.dart';
import 'package:journeytothewest/view_models/SceneViewModel.dart';
import 'package:journeytothewest/view_models/ToolViewModel.dart';
import 'package:journeytothewest/views/actor/ban_and_unban/enable_actor_screen.dart';
import 'package:journeytothewest/views/scene/home/scene_home_screen.dart';
import 'package:journeytothewest/views/tool/home/tool_home_screen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeViewModel extends Model {
  User currentUser;
  List<Items> myList;
  List<Items> showList;
  bool isCheck = false;
  bool isFilter = false;
  HomeViewModel() {
    myList = [item1, item2, item3, item4, item6, item7, item8];
  }
  Items item1 = new Items(
      title: "Enable User",
      img: "assets/icons/001-boolean.svg",
      role: "Admin",
      screen: "EnableScreen"
  );
  Items item2 = new Items(
      title: "Shooting Schedule",
      img: "assets/icons/003-to-do-list.svg",
      role: "User");
  Items item3 = new Items(
      title: "Stars",
      img: "assets/icons/005-star.svg",
      role: "Admin");
  Items item4 = new Items(
      title: "Tools",
      img: "assets/icons/003-cameraman.svg",
      role: "Admin",
      screen: "ToolScreen",
      color1: ToolColor.primaryColor,
      color2: Color(0xFF56ab2f)
  );
  Items item6 = new Items(
      title: "Characters",
      img: "assets/icons/002-sad.svg",
      role: "User");
  Items item7 = new Items(
      title: "Scenes",
      img: "assets/icons/001-story-board.svg",
      role: "Admin",
      screen: "SceneScreen",
      color1: SceneColor.primaryColor,
      color2: Color(0xFFfc4a1a),
  );
  Items item8 = new Items(
      title: "History",
      img: "assets/icons/002-history.svg",
      role: "User");

  void changeScreen(String screen, BuildContext context) {
    switch (screen) {
      case "SceneScreen":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SceneScreen(
                  model: SceneViewModel(),
                )));
        break;
      case "EnableScreen":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => EnableActorScreen(
                  model: ActorViewModel(),
                )));
        break;
      case "ToolScreen":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ToolHomeScreen(
                  model: ToolViewModel()
                )));
        break;
    }
  }
  void fetchCurrentUser(BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    UserRepository userRepository = UserRepositoryImp();
    if(token !=null){
      userRepository.fetchUser(token).then((value) {
        currentUser = value;
        print(currentUser.id);
        if(currentUser.status != StatusUser.DISABLE){
          myList = myList.where((element) => currentUser.role.contains(element.role)).toList();
          isCheck = true;
          notifyListeners();
        }
      });
    }else {
      throw Exception("Fetch User Fail");
    }
  }
  // Search filter

}
class Items {
  String title;
  String subtitle;
  String event;
  String img;
  String role;
  String screen;
  Color color1;
  Color color2;

  Items(
      {this.title,
        this.subtitle,
        this.event,
        this.img,
        this.role,
        this.screen,
      this.color1 = Colors.white,
      this.color2 = Colors.black
      });
}

