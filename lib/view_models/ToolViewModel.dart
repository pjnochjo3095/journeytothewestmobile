import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:journeytothewest/models/Tool.dart';
import 'package:journeytothewest/repos/ToolRepository.dart';
import 'package:scoped_model/scoped_model.dart';

class ToolViewModel extends Model {
  List<Tool> listTool = new List();
  bool isFetch = false;
  bool isDone = false;
  String name, desc, status;
  File image;
  int amount;
  Tool currentTool;
  File currentImage;
  String currentId;

  void pickImage() async {
    final picked = await ImagePicker.pickImage(source: ImageSource.gallery);
    image = new File(picked.path);
    notifyListeners();
  }

  void changeText(String type, String value) {
    switch (type) {
      case "desc":
        desc = value;
        break;
      case "name":
        name = value;
        break;
      case "amount":
        amount = int.parse(value);
        break;
    }
  }

  void fetchTool() {
    ToolRepository toolRepository = ToolRepositoryImp();
    toolRepository.fetchTool().then((value) {
      if (value.length >= 0) {
        listTool.clear();
        value.forEach((element) {
          listTool.add(element);
          notifyListeners();
        });
        isFetch = true;
      }
    });
  }

  void create(BuildContext context) {
    if (name != null && desc != null && image != null && amount != null) {
      ToolRepository toolRepository = ToolRepositoryImp();
      ToolCreateModel model = ToolCreateModel(name, desc, image, amount);
      toolRepository.create(model).then((value) {
        if (value) {
          Fluttertoast.showToast(
              msg: "Add Success",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              fontSize: 16.0);
          listTool.clear();
          isFetch = false;
          Navigator.of(context).pop();
          notifyListeners();
        } else {
          Fluttertoast.showToast(
              msg: "Add Fail",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              fontSize: 16.0);
        }
      });
    }
    Fluttertoast.showToast(
        msg: "Please input all",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        fontSize: 16.0);
  }

  void fetchById(String id) {
    ToolRepository toolRepository = ToolRepositoryImp();
    toolRepository.getById(id).then((value) {
      if (value != null) {
        currentTool = value;
        isDone = true;
        prepareInit();
      }
      notifyListeners();
    });
  }
  TextEditingController nameInit = TextEditingController();
  TextEditingController amountInit = TextEditingController();
  TextEditingController descInit = TextEditingController();
  void prepareInit() {
    if (currentTool != null) {
      nameInit..text = currentTool.name;
      amountInit..text = currentTool.amount.toString();
      descInit..text = currentTool.description;
      notifyListeners();
      isFetch = false;
    }
  }

  void update(BuildContext context) {
    ToolRepository toolRepository = ToolRepositoryImp();
    ToolUpdateModel model = ToolUpdateModel(
     name != null? name : currentTool.name,
      desc != null? desc : currentTool.description,
      image,
      amount != null? amount : currentTool.amount,
    );
    toolRepository.update(model, currentTool.id).then((value) {
      if (value) {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
        isFetch = false;
        listTool.clear();
        notifyListeners();
      }
    });
  }

}