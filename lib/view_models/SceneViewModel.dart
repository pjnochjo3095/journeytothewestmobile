import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:journeytothewest/models/Scene.dart';
import 'package:journeytothewest/models/Tool.dart';
import 'package:journeytothewest/repos/SceneRepository.dart';
import 'package:scoped_model/scoped_model.dart';

class SceneViewModel extends Model {
  List<Scene> listScene = new List();
  String name, desc, location;
  DateTime begin, end;
  int snapshot;
  bool isChange = false;
  Scene currentScene;
  bool isFetch = false;
  void changeText(String type, String value) {
    switch (type) {
      case "name":
        name = value;
        break;
      case "desc":
        desc = value;
        break;
      case "location":
        location = value;
        break;
      case "snapshot":
        snapshot = int.parse(value);
        break;
    }
    notifyListeners();
  }



  void setTime(String type, DateTime value) {
    switch (type) {
      case "begin":
        begin = value;
        break;
      case "end":
        end = value;
        break;
    }
    notifyListeners();
  }
  void fetchScene() {
    SceneRepository sceneRepository = SceneRepositoryImp();
    sceneRepository.fetchScene().then((value) {
      if (value.length > 0){
        listScene.clear();
        value.forEach((element) {
          listScene.add(element);
          notifyListeners();
        });
      }
      isChange = true;
    });
  }

  void create(BuildContext context) {
    SceneRepository sceneRepository = SceneRepositoryImp();
    SceneCreateModel model = new SceneCreateModel(this.name, this.desc,
        this.location, this.begin, this.end, this.snapshot);
    sceneRepository.createScene(model).then((value) {
      if (value) {
        Fluttertoast.showToast(
            msg: "Create Success",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            fontSize: 16.0);
        isChange = false;
        listScene.clear();
        Navigator.of(context).pop();
        notifyListeners();
      } else {
        Fluttertoast.showToast(
            msg: "Create Fail",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            fontSize: 16.0);
      }
    });
  }
  void delete(String id, BuildContext context) {
    SceneRepository sceneRepository = SceneRepositoryImp();
    sceneRepository.deleteScene(id).then((value) {
      if (value) {
        Fluttertoast.showToast(
            msg: "Delete Success",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            fontSize: 16.0);
      } else {
        Fluttertoast.showToast(
            msg: "Delete Fail",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            fontSize: 16.0);
      }
    });
  }
  void getSceneById(String id) {
    SceneRepository sceneRepository = SceneRepositoryImp();
    sceneRepository.getById(id).then((value) {
      if (value != null) {
        currentScene =  value;
        prepareEdit();
      }
      notifyListeners();
    });
  }
  void update (BuildContext context){
      SceneRepository sceneRepository = SceneRepositoryImp();
      SceneUpdateModel model = SceneUpdateModel(
        id: currentScene.id,
        name: this.name != null ? this.name: this.currentScene.name,
        location:this.location != null ? this.location: this.currentScene.location,
        desc:  this.desc != null ? this.desc: this.currentScene.desc,
        begin: this.begin != null ? this.begin: DateTime.parse(this.currentScene.begin),
        end:  this.end != null ? this.end: DateTime.parse(this.currentScene.end),
        snapshot:  this.snapshot != null ? this.snapshot: this.currentScene.snapshot
      );
      sceneRepository.updateScene(model).then((value) {
        if (value) {
          Fluttertoast.showToast(
              msg: "Update Success",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              fontSize: 16.0);
          listScene.clear();
          isChange = false;
          notifyListeners();
          Navigator.of(context).pop();
          Navigator.of(context).pop();
        } else {
          Fluttertoast.showToast(
              msg: "Update Fail",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              fontSize: 16.0);
        }
      });
  }
  var initName = TextEditingController();
  var initDesc = TextEditingController();
  var initLocation = TextEditingController();
  var initSnapshot = TextEditingController();
  void prepareEdit(){
      initName..text = currentScene.name;
      initDesc..text = currentScene.desc;
      initLocation..text = currentScene.location;
      initSnapshot..text = currentScene.snapshot.toString();
      isChange = false;
  }

}
