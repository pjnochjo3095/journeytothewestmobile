import 'dart:convert';

import 'package:journeytothewest/helpers/Constant.dart';
import 'package:journeytothewest/models/Scene.dart';
import 'package:http/http.dart' as http;

abstract class SceneRepository {
  Future<List<Scene>> fetchScene();

  Future<Scene> getById(String id);

  Future<bool> createScene(SceneCreateModel model);

  Future<bool> updateScene(SceneUpdateModel model);

  Future<bool> deleteScene(String id);
}

class SceneRepositoryImp with SceneRepository {
  @override
  Future<bool> createScene(SceneCreateModel model) async {
    var response = await http.post(SceneAPI.CREATE_SCENE,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        body: json.encode(<String, dynamic>{
          "name": model.name.toString(),
          "description": model.desc.toString(),
          "location": model.location.toString(),
          "time_start": model.begin.toIso8601String(),
          "time_end": model.end.toIso8601String(),
          "snapshot": model.snapshot
        }));

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Status code is: " + response.statusCode.toString());
    }
  }

  @override
  Future<bool> deleteScene(String id) async {
    var response = await http.delete(SceneAPI.DELETE_SCENE + id,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        });
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Status " + response.statusCode.toString());
    }
  }

  @override
  Future<List<Scene>> fetchScene() async {
    final respone = await http.get(SceneAPI.FETCH_LIST_SCENE,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        });
    if (respone.statusCode == 200) {
      List<dynamic> listTempt = json.decode(respone.body);
      List<Scene> result = new List<Scene>();
      if (listTempt != null) {
        for (var tempt in listTempt) {
          var map = tempt as Map<String, dynamic>;
          result.add(Scene.fromJson(map));
        }
        return result;
      } else {
        throw Exception("Fetch Film Error");
      }
    } else {
      throw Exception("Status " + respone.statusCode.toString());
    }
  }

  @override
  Future<Scene> getById(String id) async {
    var response = await http.get(SceneAPI.GET_BY_ID + id,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        });
    if (response.statusCode == 200) {
      return Scene.fromJson(json.decode(response.body));
    } else {
      throw Exception("Status " + response.statusCode.toString());
    }
  }

  @override
  Future<bool> updateScene(SceneUpdateModel model) async {
    var response = await http.put(SceneAPI.UPDATE_SCENE + model.id,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        body: json.encode(<String, dynamic>{
          "name": model.name,
          "description": model.desc,
          "location": model.location,
          "time_start": model.begin.toIso8601String(),
          "time_end": model.end.toIso8601String(),
          "snapshot": model.snapshot
        }));

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Status " + response.statusCode.toString());
    }
  }
}
