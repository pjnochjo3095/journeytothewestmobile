import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:journeytothewest/helpers/Constant.dart';
import 'package:journeytothewest/models/Actor.dart';

abstract class ActorRepository {
  Future<List<Actor>> fetchActor();

  Future<Actor> GetActorById(String id);

  Future<bool> DeleteActor(String id);

  Future<bool> EnableActor(String id);
}

class ActorRepositoryImp with ActorRepository {
  @override
  Future<bool> EnableActor(String id) async {
    final response = await http.put(ActorAPI.ENABlE_ACTOR_BY_ID + id, headers: <String, String>{
      'accept': 'application/json',
      "Content-Type": "application/json; charset=UTF-8",
    });
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Change Fail");
    }
  }

  @override
  Future<bool> DeleteActor(String id) {
    // TODO: implement DeleteActor
    throw UnimplementedError();
  }

  @override
  Future<Actor> GetActorById(String id) async {
    var response = await http.get(ActorAPI.GET_ACTOR_BY_ID + id,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        });
    if (response.statusCode == 200) {
      return Actor.fromJson(json.decode(response.body));
    } else {
      throw Exception("Not Found");
    }
  }

  @override
  Future<List<Actor>> fetchActor() async {
    final response = await http.get(ActorAPI.FETCH_LIST_ACTOR,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        });
    if (response.statusCode == 200) {
      List<dynamic> listTemp = json.decode(response.body);
      List<Actor> listActor = new List<Actor>();
      if (listTemp != null) {
        for (var elm in listTemp) {
          var map = elm as Map<String, dynamic>;
          listActor.add(Actor.fromJson(map));
        }
        return listActor;
      } else {
        throw Exception("Error List Actor is Null");
      }
    } else {
      throw Exception("Error");
    }
  }
}